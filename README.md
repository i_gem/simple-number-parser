﻿This is a simple program that reads numbers in the range from 1 to 9999, written in English and prints the minimum value once every 5 seconds.

Example:

input:
"one hundred and twelve"
"two"

output:
"2"
"112"

The program accepts one optional argument, “gui”. Used to start with a graphical interface. The console is used by default.

Maven could be used to build a package.





Простая программа, которая считывает числа в диапазоне от 1 до 9999, написанные на английском языке и печатает минимальное значение один раз в 5 секунд.

Пример:
ввод:
"one hundred twelve"
"two"

вывод:
"2"
"112"

Программа принимает один необязательный аргумент - «gui». Используется для запуска программы с графическим интерфейсом. Консоль используется по умолчанию.

Для сборки может использоваться Maven. 