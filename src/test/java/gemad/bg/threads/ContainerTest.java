package gemad.bg.threads;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContainerTest {

    @Test
    public void pushPop(){
        final Container container = new Container();
                container.pushNumber("one");
                container.pushNumber("nine hundred");
                container.pushNumber("ninety nine");
                container.pushNumber("one hundred");
                container.pushNumber("twelve");
                container.pushNumber("eight");
                container.pushNumber("two thousand seven hundred four");
                container.pushNumber("two thousand seven hundred three");
                container.pushNumber("two");
                container.pushNumber("thirty three");
                container.pushNumber("five hundred forty");

                assertEquals(false, container.isEmpty());

            assertEquals(1, container.popNumber());
            assertEquals(2, container.popNumber());
            assertEquals(8, container.popNumber());
            assertEquals(12, container.popNumber());
            assertEquals(33, container.popNumber());
            assertEquals(99, container.popNumber());
            assertEquals(100, container.popNumber());
            assertEquals(540, container.popNumber());
            assertEquals(900, container.popNumber());
            assertEquals(2703, container.popNumber());
            assertEquals(2704, container.popNumber());

            assertEquals(true, container.isEmpty());
    }
}
