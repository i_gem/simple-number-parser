package gemad.bg.threads;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PositiveNumberTest {

    private static final String[] DIGITS = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    private static final String[] TEEN = {"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    private static final String[] TENS = {"ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    private static final String HUNDRED = "hundred";
    private static final String THOUSAND = "thousand";


    //returns String array of numbers 10..99 in word format
    private String[] getTwoDigits(){
        String[] twoDigits = new String[90];

        twoDigits[0] = TENS[0];
        int i = 1;
        for (int j = 0; j < TEEN.length; j++, i++) {
            twoDigits[i] = TEEN[j];
        }

        for (int j = 1; j < TENS.length; j++) {
            twoDigits[i] = TENS[j];
            i++;
            for (int k = 0; k < DIGITS.length; k++, i++) {
                twoDigits[i] = TENS[j] + " " + DIGITS[k];
            }
        }
        return twoDigits;
    }

    //returns String array of numbers 100..999 in word format
    private String[] getThreeDigits(){
        String[] twoDigits = getTwoDigits();
        String[] threeDigits = new String[900];

        int i = 0;
        for (String DIGIT : DIGITS) {
            threeDigits[i] = DIGIT + " " + HUNDRED;
            i++;
            for (int k = 0; k < DIGITS.length; k++, i++) {
                threeDigits[i] = DIGIT + " " + HUNDRED + " " + DIGITS[k];
            }
            for (int l = 0; l < twoDigits.length; l++, i++) {
                threeDigits[i] = DIGIT + " " + HUNDRED + " " + twoDigits[l];
            }
        }
        return threeDigits;
    }

    //returns String array of numbers 1000..9999 in word format
    private String[] getFourDigits(){
        String[] twoDigits = getTwoDigits();
        String[] threeDigits = getThreeDigits();
        String[] fourDigits = new String[9000];

        int i = 0;
        for (String DIGIT : DIGITS) {
            fourDigits[i] = DIGIT + " " + THOUSAND;
            i++;
            for (int k = 0; k < DIGITS.length; k++, i++) {
                fourDigits[i] = DIGIT + " " + THOUSAND + " " + DIGITS[k];
            }
            for (int l = 0; l < twoDigits.length; l++, i++) {
                fourDigits[i] = DIGIT + " " + THOUSAND + " " + twoDigits[l];
            }
            for (int k = 0; k < threeDigits.length; k++, i++) {
                fourDigits[i] = DIGIT + " " + THOUSAND + " " + threeDigits[k];
            }
        }
        return fourDigits;
    }

    // 1, 2, .. , 9
    @Test
    public void parseDigits(){
        Number[] numbers = new Number[DIGITS.length];

        for (int i = 0; i < DIGITS.length; i++) {
            numbers[i] = new Number(DIGITS[i]);
        }

        for (int i = 0; i < numbers.length; i++) {
            assertEquals(true, Number.validValue(numbers[i].getRepresentation()));
            assertEquals(i + 1, numbers[i].getValue());
        }
    }

    //10, 20, .. , 90
    @Test
    public void parseTens(){
        Number[] numbers = new Number[TENS.length];

        for (int i = 0; i < TENS.length; i++) {
            numbers[i] = new Number(TENS[i]);
        }

        for (int i = 0; i < numbers.length; i++) {
            assertEquals(true, Number.validValue(numbers[i].getRepresentation()));
            assertEquals((i + 1) * 10, numbers[i].getValue());
        }
    }

    //100, 200, .. , 900
    @Test
    public void parseHundreds(){
        Number[] numbers = new Number[DIGITS.length];

        for (int i = 0; i < DIGITS.length; i++) {
            numbers[i] = new Number(DIGITS[i] + " " + HUNDRED);
        }

        for (int i = 0; i < numbers.length; i++) {
            assertEquals(true, Number.validValue(numbers[i].getRepresentation()));
            assertEquals((i + 1) * 100, numbers[i].getValue());
        }
    }

    //1000, 2000, .. , 9000
    @Test
    public void parseThousands(){
        Number[] numbers = new Number[DIGITS.length];

        for (int i = 0; i < DIGITS.length; i++) {
            numbers[i] = new Number(DIGITS[i] + " " + THOUSAND);
        }

        for (int i = 0; i < numbers.length; i++) {
            assertEquals(true, Number.validValue(numbers[i].getRepresentation()));
            assertEquals((i + 1) * 1000, numbers[i].getValue());
        }
    }

    //11..19
    @Test
    public void parseTeen(){
        Number[] numbers = new Number[9];

        for (int i = 0; i < TEEN.length; i++) {
            numbers[i] = new Number(TEEN[i]);
        }

        for (int i = 0; i < numbers.length; i++) {
            assertEquals(true, Number.validValue(numbers[i].getRepresentation()));
            assertEquals((i + 1) + 10, numbers[i].getValue());
        }
    }

    //21..29, 31..39, ... , 91..99
    @Test
    public void parseTwoDigit(){
        Number[] numbers = new Number[DIGITS.length * (TENS.length - 1)];
        int j = 0; // for position in TENS
        for (int i = 0; i < numbers.length; i++) {
            int k = i % 9; // for position in DIGITS
            if (k == 0)
                j++;
            numbers[i] = new Number(TENS[j] + " " + DIGITS[k]);
        }
        int num = 21;
        for (Number number : numbers) {
            assertEquals(true, Number.validValue(number.getRepresentation()));
            assertEquals(num, number.getValue());
            num++;
            if (num % 10 == 0)
                num++;
        }
    }

    //100..999
    @Test
    public void parseThreeDigit(){
        String[] threeDigit = getThreeDigits();
        Number[] threeD = new Number[threeDigit.length];
        for (int i = 0; i < threeD.length; i++) {
            threeD[i] = new Number(threeDigit[i]);
        }
        for (int i = 0; i < threeD.length; i++) {
            assertEquals(true, Number.validValue(threeD[i].getRepresentation()));
            assertEquals(i + 100, threeD[i].getValue());
        }
    }

    //1000..9999
    @Test
    public void parseFourDigit(){
        String[] fourDigit = getFourDigits();
        Number[] fourD = new Number[fourDigit.length];
        for (int i = 0; i < fourDigit.length; i++) {
            fourD[i] = new Number(fourDigit[i]);
        }
        for (int i = 0; i < fourD.length; i++) {
            assertEquals(true, Number.validValue(fourD[i].getRepresentation()));
            assertEquals(i + 1000, fourD[i].getValue());
        }
    }



}
