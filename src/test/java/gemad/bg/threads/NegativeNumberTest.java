package gemad.bg.threads;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NegativeNumberTest {

    @Test
    public void notNumbers() {
        assertEquals(false, Number.validValue(""));
        assertEquals(false, Number.validValue("test"));
        assertEquals(false, Number.validValue("ohne huuundred"));
    }

    @Test
    public void wrongOrder() {
        assertEquals(false, Number.validValue("ten one hundred"));
        assertEquals(false, Number.validValue("twelve two"));
        assertEquals(false, Number.validValue("fifty fifty"));
        assertEquals(false, Number.validValue("one one one"));
        assertEquals(false, Number.validValue("four four"));
        assertEquals(false, Number.validValue("one thousand two thousand"));
        assertEquals(false, Number.validValue("seven ten"));
        assertEquals(false, Number.validValue("one hundred fifty twelve"));
        assertEquals(false, Number.validValue("nine thousand one eleven"));
    }

    @Test
    public void hundredsThousands() {
        assertEquals(false, Number.validValue("hundred"));
        assertEquals(false, Number.validValue("thousand"));
        assertEquals(false, Number.validValue("ten thousand"));
        assertEquals(false, Number.validValue("hundred hundred"));
    }

    @Test
    public void extraWords() {
        assertEquals(false, Number.validValue("one hundred and nine"));
        assertEquals(false, Number.validValue("twenty one zero"));
    }
}
