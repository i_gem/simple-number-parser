package gemad.bg.threads;


import gemad.bg.threads.forms.MainForm;
import gemad.bg.threads.runnable.Consumer;
import gemad.bg.threads.runnable.Producer;
import gemad.bg.threads.runnable.ProducerGUI;

public class Starter {
    //gui argument tells it to use JFrame, console is used by default
    public static void main(String[] args) {
        Starter starter = new Starter();
        boolean gui = false;
        if (args.length > 0)
            gui = "gui".equals(args[0]);// gui or console
        starter.go(gui);
    }

    public void go(boolean gui){

        Container container = new Container();


        if (gui){
            MainForm form = new MainForm("Simple Number Parser",container);
            Thread thread = new Thread(new ProducerGUI(container, form));
            thread.start();
        } else {
            Thread thread2 = new Thread(new Producer(container));
            Thread thread1 = new Thread(new Consumer(container));
            thread1.start();
            thread2.start();
            System.out.println("Awaiting input");
        }
    }
}
