package gemad.bg.threads;

import java.util.HashMap;

public class Dictionary {
    private HashMap<String, Integer> values;
    private static Dictionary dictionary;

    public static Dictionary getInstance(){
        if (dictionary == null)
            dictionary = new Dictionary();
        return dictionary;
    }

    private Dictionary(){
        values = new HashMap<>();
        values.put("one", 1);
        values.put("two", 2);
        values.put("three", 3);
        values.put("four", 4);
        values.put("five", 5);
        values.put("six", 6);
        values.put("seven", 7);
        values.put("eight", 8);
        values.put("nine", 9);
        values.put("ten", 10);
        values.put("eleven", 11);
        values.put("twelve", 12);
        values.put("thirteen", 13);
        values.put("fourteen", 14);
        values.put("fifteen", 15);
        values.put("sixteen", 16);
        values.put("seventeen", 17);
        values.put("eighteen", 18);
        values.put("nineteen", 19);
        values.put("twenty", 20);
        values.put("thirty", 30);
        values.put("forty", 40);
        values.put("fifty", 50);
        values.put("sixty", 60);
        values.put("seventy", 70);
        values.put("eighty", 80);
        values.put("ninety", 90);
        values.put("hundred", 100);
        values.put("thousand", 1000);
    }

    //returns value for given String or -1 if no corresponding String found
    public int getValue(String word){
        int result = -1;
        if (values.containsKey(word)){
            result = values.get(word);
        }
        return result;
    }
}
