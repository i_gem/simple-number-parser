package gemad.bg.threads.runnable;

import gemad.bg.threads.Container;
import gemad.bg.threads.forms.IOForm;


public class ProducerGUI extends IORunnable {

    IOForm frame;
    public ProducerGUI(Container c, IOForm frame) {
        super(c);
        this.frame = frame;
    }

    @Override
    public void run() {
        while (true){
            if (!container.isEmpty()) {
                frame.addLine(container.popNumber() + "");
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
