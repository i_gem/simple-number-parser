package gemad.bg.threads.runnable;


import gemad.bg.threads.Container;

import java.util.Scanner;

//reads numbers from console and puts them into the container
public class Consumer extends IORunnable {
    private Scanner scanner;
    public Consumer(Container container) {
        super(container);
        scanner = new Scanner(System.in);
    }

    public void run() {
        while (true) {
            container.pushNumber(scanner.nextLine());
        }
    }
}
