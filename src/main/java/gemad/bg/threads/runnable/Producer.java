package gemad.bg.threads.runnable;


import gemad.bg.threads.Container;
import gemad.bg.threads.runnable.IORunnable;

//prints number into the console from container once in 5 seconds
public class Producer extends IORunnable {
    public Producer(Container container) {
        super(container);
    }

    public void run() {
        while (true) {
            if (!container.isEmpty()) {
                System.out.println(container.popNumber());
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
