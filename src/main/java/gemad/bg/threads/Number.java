package gemad.bg.threads;

//class for parsing and storing a number
public class Number implements Comparable<Number> {
    private int value = 0;
    private String representation = "";

    public Number(String num) throws IllegalArgumentException{
        representation = num;
        if (!validValue(num))
            throw new IllegalArgumentException("Incorrect number value. Should be number from \"one\" to \"nine thousand nine hundred ninety nine\"");
        value = parseNum(num);
    }

    //input - string that represents a number written in words "one" to "nine thousand nine hundred ninety nine"
    //output - number
    private int parseNum(String numLine){
        int previous = 0;
        int result = 0;
        Dictionary dictionary = Dictionary.getInstance();
        String[] arr = numLine.split(" ");
        for (String word : arr) {
            int value = dictionary.getValue(word); //if no mapping to current word found, it skips it
            if (value == -1)
                continue;
            if (value % 100 == 0) //if hundreds or thousands
                if (value > previous && previous > 0) {
                    result += previous * value;
                    previous = 0;
                } else {
                    result += value;
                }
            else { //no validation here, just parsing
                previous += value;
            }

        }
        result += previous;
        return result;
    }

    public static boolean validValue(String number){
        String[] words = number.split(" ");
        int digit = 4; //current possible digit
        boolean hasPrev = false; // for digits before hundreds and thousands
        Dictionary dictionary = Dictionary.getInstance();
        for (int i = 0; i < words.length; i++) {
            int value = dictionary.getValue(words[i]);
            if (value == -1)
                return false;
            if (value < 10) {
                if (hasPrev)
                    return false; // for example, "one two" or "nine nine"
                hasPrev = true;
                continue;
            }
            if (value % 100 == 0) {
                if (!hasPrev)
                    return false;
                else {
                    if (digit >= 4 && value == 1000)
                        digit = 3;
                    else if (digit >= 3 && value == 100) {
                        digit = 2;
                    } else {
                        return false; //return false if there is no way hundreds or thousands can be at that point
                    }
                }
            } else
            if (value < 20){
                return !(digit < 2 || hasPrev || i + 1 < words.length);
            } else
                if (digit < 2 || hasPrev)
                    return false;
            else {
                digit = 1;
                }
            hasPrev = false;
        }
        return true;
    }

    public int getValue(){
        return value;
    }

    public String getRepresentation(){
        return representation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Number)) return false;

        Number number = (Number) o;

        if (value != number.value) return false;
        return representation != null ? representation.equals(number.representation) : number.representation == null;
    }

    @Override
    public int hashCode() {
        int result = value;
        result = 31 * result + (representation != null ? representation.hashCode() : 0);
        return result;
    }

    public int compareTo(Number o) {
        return this.value - o.value;
    }
}
