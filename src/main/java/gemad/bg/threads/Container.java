package gemad.bg.threads;

import java.util.PriorityQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//class for storing all numbers in order
public class Container {
    private PriorityQueue<Integer> numbers;

    public Container() {
        numbers = new PriorityQueue<>();
    }

    public boolean isEmpty() {
        return numbers.isEmpty();
    }

    //retrieves first element from the numbers collection
    public int popNumber() {
        synchronized (numbers) {
            while (numbers.isEmpty())
                try {
                    numbers.wait();
                } catch (InterruptedException e) {
                    System.out.println("Thread interrupted");
                }
            return numbers.poll();
        }
    }

    //parses and pushes value into the numbers collection
    public void pushNumber(String number) {
        if (!Number.validValue(number)) {
            System.out.println("Incorrect input");
            return;
        }
        Number n = new Number(number);
        if (n.getValue() == 0) {
            return;
        }
        synchronized (numbers) {
            numbers.add(n.getValue());
            numbers.notify();
        }
    }
}
