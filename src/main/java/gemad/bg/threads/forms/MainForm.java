package gemad.bg.threads.forms;

import gemad.bg.threads.Container;
import gemad.bg.threads.Number;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;


public class MainForm extends JFrame implements IOForm{

    private JPanel rootPanel;
    private JPanel inputPanel;
    private JPanel messagePanel;
    private JPanel outputPanel;
    private JTextField inputField;
    private JButton buttonSend;
    private JLabel errorLabel;
    private JScrollPane scrollPane;
    private JTextArea outputArea;

    Container container;

    public MainForm(String title, Container container){
        this.container = container;
        this.setTitle(title);
        init();
    }

    private void init(){
        inputPanel = new JPanel(new BorderLayout());
        messagePanel = new JPanel(new BorderLayout());
        outputPanel = new JPanel(new BorderLayout());
        rootPanel = new JPanel(new BorderLayout());
        inputField = new JTextField();
        buttonSend = new JButton("Send");
        errorLabel = new JLabel(" ");
        outputArea = new JTextArea();
        scrollPane = new JScrollPane(outputArea);

        Box inputBox = Box.createHorizontalBox();
        inputPanel.add(inputBox);
        inputBox.add(inputField);
        inputBox.add(buttonSend);
        inputField.setMaximumSize(new Dimension(Integer.MAX_VALUE, 400));

        messagePanel.add(errorLabel);
        messagePanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 400));

        outputPanel.add(scrollPane);

        Box rootBox = Box.createVerticalBox();
        rootPanel.add(rootBox);
        rootBox.add(inputBox);
        rootBox.add(messagePanel);
        rootBox.add(outputPanel);

        rootPanel.setBorder(new EmptyBorder(20, 10, 20, 10));
        this.setContentPane(rootPanel);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        buttonSend.addActionListener(e -> {
            String text = inputField.getText();
            if (Number.validValue(text)){
                container.pushNumber(text);
                wrongInput(false);
            } else {
                wrongInput(true);
            }
            inputField.setText("");
        });
        this.getRootPane().setDefaultButton(buttonSend);
        this.pack();
        this.setBounds(500, 250, 800, 500);
        this.setVisible(true);
    }


    public void addLine(String line){
        outputArea.append(line + "\n");
    }

    public void wrongInput(boolean b){
        if (b){
            errorLabel.setText("Wrong value!");
        } else {
            errorLabel.setText(" ");
        }
    }

}
